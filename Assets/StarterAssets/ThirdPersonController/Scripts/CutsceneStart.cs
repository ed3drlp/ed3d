using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

[RequireComponent(typeof(MeshCollider))]
[RequireComponent(typeof(SignalReceiver))]
public class CutsceneStart : CutsceneInteract
{
    [SerializeField] private GameObject _cutscene1;
    [SerializeField] private GameObject _cutscene2;
    // Start is called before the first frame update
    

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Activate()
    {
        base.Activate();
    }

    public override void Deactivate()
    {
        base.Deactivate();
    }
}
