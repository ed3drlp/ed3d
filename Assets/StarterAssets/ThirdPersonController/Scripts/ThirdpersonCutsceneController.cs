using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine;

public class ThirdpersonCutsceneController : MonoBehaviour
{
    public static ThirdpersonCutsceneController Instance;
    [SerializeField] private StarterAssets.ThirdPersonController _Controller;
    // Start is called before the first frame update

    private void Awake()
    {
        //para instanciar el propio Script
        Instance = this;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //para activar y desactivar las cosas del script
    public void Activate()
    {
        _Controller.enabled = true;
    }

    public void Deactivate()
    {
        _Controller.enabled = false;
    }
}
