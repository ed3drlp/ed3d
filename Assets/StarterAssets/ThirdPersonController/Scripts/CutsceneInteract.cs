using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CutsceneInteract : MonoBehaviour
{
    private bool _playerWithinRange;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_playerWithinRange)
        {
            Activate();
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            _playerWithinRange = true;
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            _playerWithinRange = false;
        }
    }

    public virtual void Activate()
    {

    }

    public virtual void Deactivate()
    {

    }
}
